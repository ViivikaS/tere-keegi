﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UusTest2502
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Tere!");

            int arv = 7;
            int teinearv;
            teinearv = 10;

            Console.WriteLine((arv = teinearv) * 7);
            int kolmasarv = (arv * 2);
            kolmasarv += (arv += 7) * 2;
            Console.WriteLine(kolmasarv);

            arv = 20;
            Console.WriteLine(arv++);
            Console.WriteLine(--arv);

            Console.WriteLine(5 & 6); // trükib 4 sest tehe biti tasandil 6 - 0110 ja 5 - 0101 5 & 6 0 ja 0=0 1 ja 1 = 1 1 ja 0=0  ja 0 ja 1=0 tulemus 0100 = 4
            Console.WriteLine(5 ^ 6); // trükib 7
            Console.WriteLine(5 | 6); // trükib 3 ^ on bitwise xor tehe
            // bitwise logical operations

            


            string nimi = "Viivika";
            Console.WriteLine(nimi[1] + 0); // +0 teeb stringist int'i

            string failinimi = "c:\\hobune\\nimi"; // @"C:\ @ võimaldab kasutada \ märki failinimes
            Console.WriteLine("Tere \n head aega!"); // \n on line feed
            Console.WriteLine("Tere \r Head aega!"); // \r carriage return ehk viib rea algusesse ja kirjutab üle

            string nimi2 = "viivika";

            Console.WriteLine(nimi.Equals(nimi2, StringComparison.CurrentCultureIgnoreCase) ? "samad" : "erinevad"); // ignore case ei vaata

            int arv1 = 7;
            decimal arv2 = 10.777M;

            // kõik neli on sama lahendus
            Console.WriteLine("arvud on sellised {0} ja {1:F2}", arv1, arv2); // F2=määrab koma kohad {} on place holder

            string tulemus = string.Format("arvud on sellised {0} ja {1:F2}", arv1, arv2);
            Console.WriteLine(tulemus);

            Console.WriteLine($"arvud on sellised {arv1} ja {arv2:F2}"); // $=interpoleeritud string
            tulemus = $"arvud on sellised {arv1} ja {arv2:F2}";
            Console.WriteLine(tulemus);

            #region kommenteerisin välja
            // Muutuja var näited
            var v = 7;
            Console.WriteLine(v.GetType().Name); // Name ja FullName saab kasutada GetType'iga
            var teine = v; //mõlemad muutujad on sama tüüpi

            var henn = new { Nimi = "Henn", Vanus = 64 };
            Console.WriteLine(henn.GetType().Name);

            foreach (var x in henn.GetType().GetProperties()) Console.WriteLine(x.Name);
            #endregion

            // arvude teisendamine teisteks arvudeks
            int i = 1000;
            long l = i; // toimub ilmutamata tüübiteisendus

            i = (int)(l + 1 * 2); // ilmutatud tüübiteisendus e casting

            // tehete prioriteedid! 

            l = long.MaxValue;
            i = (int)l;
            Console.WriteLine(i); // tulemus on -1 kuna tekib overflow seda saab vältida 
            double d = 10.0;

            string tekst = "100";
            //i = (int)tekst; // see ei tööta sest stringi EI SAA CASTIDA
            //tekst = d; // see ka ei tööta

            tekst = d.ToString();
            i = int.Parse(tekst); // see teisendab teksti int asjaks
            DateTime dt = DateTime.Parse("7. märts 1955");
            Console.WriteLine(dt);

            Console.Write("Millal sa sündinud oled: ");
            string sünniaeg = Console.ReadLine();
            //DateTime sünnipäev = DateTime.Parse(sünniaeg);
            //Console.WriteLine($"sa oled siis sündinud sellisel päeval {sünnipäev.DayOfWeek}");

            if(DateTime.TryParse(sünniaeg, out DateTime sündinud))
            {
                Console.WriteLine("sinu sünnipäev on siis " + sündinud.ToShortDateString());
            }
            else Console.WriteLine("see pole miski sünniaeg");
        }
    }
    }
